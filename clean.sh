#!/usr/bin/env bash

echo "Purge o* e* results and logs"
rm -rf results
rm -f rm *.o*
rm -f rm *.e*
rm -f *.log
rm -f .nextflow.log*
rm -rf .nextflow
